#' The application User-Interface
#' 
#' @param request Internal parameter for `{shiny}`. 
#'     DO NOT REMOVE.
#' @import shiny
#' @noRd
app_ui <- function(request) {
  tagList(
    
    # Leave this function for adding external resources
    golem_add_external_resources(),
    
    htmlTemplate(
      filename = app_sys("app/www/index.html")
    ),
    
    mod_ols_ui("ols")
  )
}

#' Add external Resources to the Application
#' 
#' This function is internally used to add external 
#' resources inside the Shiny application. 
#' 
#' @import shiny
#' @importFrom golem add_resource_path activate_js favicon bundle_resources
#' @noRd
golem_add_external_resources <- function(){
  
  add_resource_path(
    'www', app_sys('app/www')
  )
  
  add_resource_path(
    "js", app_sys("app/js")
  )
 
  tags$head(
    
    favicon(),
    
    bundle_resources(
      path = app_sys('app/www'),
      app_title = 'linear.regression.app'
    ),
    
    # Add here other external resources
    # for example, you can add shinyalert::useShinyalert() 
    # tags$script(src = "js/require.js", `data-main` = "js/config.js"),
    tags$script(src = "js/anim.js"),
    tags$script(src = "js/mod-draw.js"),
    tags$script(src = "js/mod-ols.js"),
    tags$script(src = "js/main.js")
    
  )
}

