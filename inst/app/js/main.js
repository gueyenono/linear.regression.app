$(document).on("shiny:connected", function() {

  // ~~*~~ Node elements ~~*~~ //

  const canvas = document.querySelector("#graph-draw-app")
  const ctx = canvas.getContext("2d")
  const appPic = document.querySelectorAll(".app-pic")
  const tableCoords = document.querySelector("#table-coords")
  const xDisplayElement = document.querySelector("#mouse-x-coord") // Display for current x coordinate of mouse
  const yDisplayElement = document.querySelector("#mouse-y-coord") // Display for current y coordinate of mouse
  const spanIntercept = document.querySelector("#intercept") // Display OLS intercept
  const spanSlopeCoef = document.querySelector("#slope-coef") // Display OLS slope coefficient

  // ~~*~~ Initialize variables ~~*~~ //

  // grid parameters
  let width_in_px = 800
  let height_in_px = 800
  let num_lines_x = 20
  let num_lines_y = 20
  let xmin = -100
  let xmax = 100
  let ymin = -100
  let ymax = 100
  let x_axis_distance_grid_lines = 10
  let y_axis_distance_grid_lines = 10
  let x_axis_starting_point = 10
  let y_axis_starting_point = 10

  //  data object
  let coordinatesXY = []
  let coordinatesCanvas = []

  // stats results
  let statsResults

  // ~~*~~ Animate app pics ~~*~~ //

  appPic.forEach(pic => {
    pic.addEventListener("mousedown", toggleClass)
    pic.addEventListener("mouseup", toggleClass)
  })


  // ~~*~~ Draw Grid on launch ~~*~~ //

  drawGrid(
    canvas, // canvas element
    ctx, // 2D context of canvas
    width_in_px, // Canvas width in pixels
    height_in_px, // Canvas height in pixels
    num_lines_x, // Desired number of x lines in the grid
    num_lines_y, // Desired number of y lines in the grid
    x_axis_distance_grid_lines, // distance of main x axis from the top in terms of grid blocks (e.g. if 10, then the main x axis will be 10 grid blocks from the top.)
    y_axis_distance_grid_lines, // distance of main y axis from the left in terms of grid blocks (e.g. if 10, then the main y axis will be 10 grid blocks from the left.)
    x_axis_starting_point, // first tick label (after origin) on the x axis
    y_axis_starting_point // first tick label (after origin) on the y axis
  )


  // ~~*~~ Display mouse coordinates on hover ~~*~~ //

  canvas.addEventListener("mousemove", function(event) {

    let coords = toXYCoordinates(
      event.offsetX,
      event.offsetY,
      canvas,
      num_lines_x,
      num_lines_y,
      x_axis_distance_grid_lines,
      y_axis_distance_grid_lines,
      xmax,
      ymax
    )

    xDisplayElement.innerHTML = coords[0]
    yDisplayElement.innerHTML = coords[1]
  })


    // ~~*~~ When the user clicks on canvas ~~*~~ //

  canvas.addEventListener("mousedown", function(event) {

    // Store new point coordinates

    // Canvas coordinates
    coordinatesCanvas.push(
      {x: event.offsetX, y: event.offsetY}
    )

    // XY coordinates
    let coordsXY = toXYCoordinates(
      event.offsetX,
      event.offsetY,
      canvas,
      num_lines_x,
      num_lines_y,
      x_axis_distance_grid_lines,
      y_axis_distance_grid_lines,
      xmax,
      ymax
    )

    coordinatesXY.push(
      {x: coordsXY[0], y: coordsXY[1]}
    )

    console.log(coordsXY)

    // Add new data to table
    displayData(tableCoords, coordsXY)

    // CLear and redraw grid
    ctx.clearRect(0, 0, canvas.width, canvas.height)

    drawGrid(
      canvas,
      ctx,
      width_in_px,
      height_in_px,
      num_lines_x,
      num_lines_y,
      x_axis_distance_grid_lines,
      y_axis_distance_grid_lines,
      x_axis_starting_point,
      y_axis_starting_point
    )

    // Draw points on canvas
    drawPointsfromObject(
      coordinatesCanvas,
      ctx
    )

    // Sent point coordinates to R
    Shiny.setInputValue("data_coords", JSON.stringify(coordinatesXY))

    // Get statistical results from R and use them
    Shiny.addCustomMessageHandler("estimates", function(mod){

      // Display OLS results
      spanIntercept.innerHTML = Number(mod.intercept).toFixed(3)
      spanSlopeCoef.innerHTML = Number(mod.slopeCoef).toFixed(3)

      // Draw line of best fit
      drawFittedLine(
        canvas,
        ctx,
        Number(mod.intercept),
        Number(mod.slopeCoef),
        num_lines_x,
        num_lines_y,
        x_axis_distance_grid_lines,
        y_axis_distance_grid_lines,
        xmin,
        xmax,
        ymax
      )

    })

  })



})
