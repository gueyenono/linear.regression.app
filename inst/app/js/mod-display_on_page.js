// FUNCTIONS FOR DISPLAYING ON THE WEBPAGE AND OTHER ANIMATIONS

// ~~*~~ DEPENDENCIES ~~*~~ //

// toXYCoordinates()

// Display current mouse coordinates
function displayMouseCoords(clickEvent, xDisplayElement, yDisplayElement) {
  let xyCoords = toXYCoordinates(clickEvent.offsetX, clickEvent.offsetY)
  xDisplayElement.innerHTML =  xyCoords[0].toFixed(2)
  yDisplayElement.innerHTML = xyCoords[1].toFixed(2)
}
