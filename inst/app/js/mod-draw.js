// FUNCTIONS FOR DRAWING AN XY COORDINATE SYSTEM, POINTS AND LINES

// ~~*~~ DEPENDENCIES ~~*~~ //

// ~~*~~ CONVERT COORDINATES ~~*~~ //

// Convert canvas coordinates to XY coordinates
function toXYCoordinates(
  canvasXCoord, // x-coordinate of point (canvas)
  canvasYCoord, // y-coordinatre of point (canvas)
  canvas, // canvas
  num_lines_x, // number of horizontal lines on the grid
  num_lines_y, // number of vertical lines on the grid
  x_axis_distance_grid_lines, // distance of main x axis from the top in terms of grid blocks (e.g. if 10, then the main x axis will be 10 grid blocks from the top.)
  y_axis_distance_grid_lines, // distance of main y axis from the left in terms of grid blocks (e.g. if 10, then the main y axis will be 10 grid blocks from the left.)
  xmax, // maximum value on x axis
  ymax // maximum value on y axis
) {
  let propX = x_axis_distance_grid_lines / num_lines_x
  let propY = y_axis_distance_grid_lines / num_lines_y
  let xCoord = ((canvasXCoord-propX*canvas.width) * xmax) / (propX*canvas.width)
  let yCoord = -((canvasYCoord-propY*canvas.height) * ymax) / (propY*canvas.height)
  return [xCoord, yCoord]
}

// Convert XY coordinates to canvas coordinates
function toCanvasCoordinates(
  xCoord, // x-coordinate of point (according to grid)
  yCoord, // y-coordinate of point (according to grid)
  canvas, // canvas
  num_lines_x, // number of horizontal lines on the grid
  num_lines_y, // number of vertical lines on the grid
  x_axis_distance_grid_lines, // distance of main x axis from the top in terms of grid blocks (e.g. if 10, then the main x axis will be 10 grid blocks from the top.)
  y_axis_distance_grid_lines, // distance of main y axis from the left in terms of grid blocks (e.g. if 10, then the main y axis will be 10 grid blocks from the left.)
  xmax, // maximum value on x axis
  ymax // maximum value on y axis
) {
  let propX = x_axis_distance_grid_lines / num_lines_x
  let propY = y_axis_distance_grid_lines / num_lines_y
  let canvasXCoord = ((propX*xCoord*canvas.width)/xmax) + (propX*canvas.width)
  let canvasYCoord = ((propY*yCoord*canvas.height)/ymax) + (propY*canvas.height)
  return [canvasXCoord, canvasYCoord]
}


// ~~*~~ DRAW BASE GRID ~~*~~ //

// Draw grid
function drawGrid(
  canvas, // canvas element
  ctx, // 2D context of canvas
  width_in_px, // Canvas width in pixels
  height_in_px, // Canvas height in pixels
  num_lines_x, // Desired number of x lines in the grid
  num_lines_y, // Desired number of y lines in the grid
  x_axis_distance_grid_lines, // distance of main x axis from the top in terms of grid blocks (e.g. if 10, then the main x axis will be 10 grid blocks from the top.)
  y_axis_distance_grid_lines, // distance of main y axis from the left in terms of grid blocks (e.g. if 10, then the main y axis will be 10 grid blocks from the left.)
  x_axis_starting_point, // first tick label (after origin) on the x axis
  y_axis_starting_point // first tick label (after origin) on the y axis
) {

  // Set canvas dimensions
  canvas.width = width_in_px
  canvas.height = height_in_px

  // Variable declaration
  const sideLength = canvas.width // (value set in stone) Canvas height and length (in pixels)
  let grid_size = sideLength / num_lines_x


  // Draw horizontal lines with specifications
  drawXLines(
    canvas,
    ctx,
    num_lines_x,
    x_axis_distance_grid_lines,
    grid_size
  )

  // Draw vertical lines with specifications
  drawYLines(
    canvas,
    ctx,
    num_lines_y,
    y_axis_distance_grid_lines,
    grid_size
  )

  // Draw horizontal ticks
  drawXTicks(
    ctx,
    num_lines_y,
    x_axis_starting_point,
    y_axis_distance_grid_lines,
    grid_size
  )

  // Draw vertical ticks
  drawYTicks(
    ctx,
    num_lines_x,
    y_axis_starting_point,
    x_axis_distance_grid_lines,
    grid_size
  )

}

// Draw horizontal lines
function drawXLines(
  canvas, // canvas
  ctx, // 2d context of canvas
  num_lines_x, // number of horizontal lines on the grid
  x_axis_distance_grid_lines, // distance of main x axis from the top in terms of grid blocks (e.g. if 10, then the main x axis will be 10 grid blocks from the top.)
  grid_size // Length (in pixels) of one cell in the grid
) {

  for(let i = 0; i <= num_lines_x; i++){

    ctx.beginPath()
    ctx.lineWidth = 1

    if(i === x_axis_distance_grid_lines) ctx.strokeStyle = "#000000" // If line represents x-axis, draw in different color
    else ctx.strokeStyle = "#e9e9e9"

    if(i === num_lines_x) {
      ctx.moveTo(0, grid_size*i)
      ctx.lineTo(canvas.width, grid_size*i)
    } else {
      ctx.moveTo(0, grid_size*i + 0.5)
      ctx.lineTo(canvas.width, grid_size*i + 0.5)
    }

    ctx.stroke()
  }
}

// Draw vertical lines
function drawYLines(
  canvas, // canvas
  ctx, // 2d context of canvas
  num_lines_y, // number of vertical lines on the grid
  y_axis_distance_grid_lines, // distance of main y axis from the left in terms of grid blocks (e.g. if 10, then the main y axis will be 10 grid blocks from the left.)
  grid_size // Length (in pixels) of one cell in the grid
) {
  for(let i = 0; i <= num_lines_y; i++){

    ctx.beginPath()
    ctx.lineWidth = 1

    // If line represents x-axis, draw in different color
    if(i === y_axis_distance_grid_lines) ctx.strokeStyle = "#000000"
    else ctx.strokeStyle = "#e9e9e9"

    if(i === num_lines_y) {
      ctx.moveTo(grid_size*i, 0)
      ctx.lineTo(grid_size*i, canvas.height)
    } else {
      ctx.moveTo(grid_size*i + 0.5, 0)
      ctx.lineTo(grid_size*i + 0.5, canvas.height)
    }

    ctx.stroke()
  }
}

// Draw horizontal tick marks
function drawXTicks(
  ctx, // 2d context of canvas
  num_lines_y, // number of vertical lines on the grid
  x_axis_starting_point, // first tick label (after origin) on the x axis
  y_axis_distance_grid_lines, // distance of main y axis from the left in terms of grid blocks (e.g. if 10, then the main y axis will be 10 grid blocks from the left.)
  grid_size // Length (in pixels) of one cell in the grid
) {

  let y_axis_distance_px = y_axis_distance_grid_lines*grid_size

  // Tick marks along the positive x-axis
  for(let i = 1; i < (num_lines_y - y_axis_distance_grid_lines); i++) {

    let tick_coord_x = y_axis_distance_px + grid_size*i

    ctx.beginPath()
    ctx.lineWidth = 1
    ctx.strokeStyle = "#000000"

    // Each tick mark is 6px long (-3 to 3)
    ctx.moveTo(tick_coord_x + 0.5, y_axis_distance_px-3)
    ctx.lineTo(tick_coord_x + 0.5, y_axis_distance_px+3)
    ctx.stroke()

    // Text value at that point
    ctx.font = "9px Arial"
    // ctx.textAlign = "start"
    ctx.fillText(x_axis_starting_point*i, // text
      tick_coord_x - 1, // x-coordinate
      y_axis_distance_px + 15) // y-coordinate

    }

    // Tick marks along the negative x-axis
    for(let i=1; i < y_axis_distance_grid_lines; i++) {

      ctx.beginPath()
      ctx.lineWidth = 1
      ctx.strokeStyle = "#000000"

      // Each tick mark is 6px long (-3 to 3)
      ctx.moveTo(grid_size*i + 0.5, y_axis_distance_px-3)
      ctx.lineTo(grid_size*i + 0.5, y_axis_distance_px+3)
      ctx.stroke()

      // Text value at that point
      ctx.font = "9px Arial"
      // ctx.textAlign = "end"
      ctx.fillText(-(y_axis_distance_grid_lines-i)*x_axis_starting_point, // text
      grid_size*i - 3, // x-coordinate
      y_axis_distance_px + 15) // y-coordinate
    }
  }

  // Draw vertical tick marks
  function drawYTicks(
    ctx, // 2d context of canvas
    num_lines_x, // number of horizontal lines on the grid
    y_axis_starting_point, // first tick label (after origin) on the y axis
    x_axis_distance_grid_lines, // distance of main x axis from the top in terms of grid blocks (e.g. if 10, then the main x axis will be 10 grid blocks from the top.)
    grid_size // Length (in pixels) of one cell in the grid
  ) {

    let x_axis_distance_px = x_axis_distance_grid_lines * grid_size

    // Tick marks along the negative y-axis
    for(let i = 1; i < (num_lines_x - x_axis_distance_grid_lines); i++) {

      let tick_coord_y = x_axis_distance_px + grid_size*i

      ctx.beginPath()
      ctx.lineWidth = 1
      ctx.strokeStyle = "#000000"

      // Each tick mark is 6px long (-3 to 3)
      ctx.moveTo(x_axis_distance_px-3, tick_coord_y+0.5)
      ctx.lineTo(x_axis_distance_px+3, tick_coord_y+0.5)
      ctx.stroke()

      // Text value at that point
      ctx.font = "9px Arial"
      // ctx.textAlign = "start"
      ctx.fillText(-i*y_axis_starting_point, // text
        x_axis_distance_px+8, // x-coordinate
        tick_coord_y+3) // y-coordinate

      }

      // Tick marks along the positive y-axis
      for(let i = 1; i < x_axis_distance_grid_lines; i++) {

        ctx.beginPath()
        ctx.lineWidth = 1
        ctx.strokeStyle = "#000000"

        // Each tick mark is 6px long (-3 to 3)
        ctx.moveTo(x_axis_distance_px-3, grid_size*i + 0.5)
        ctx.lineTo(x_axis_distance_px+3, grid_size*i + 0.5)
        ctx.stroke()

        // Text value at that point
        ctx.font = "9px Arial"
        // ctx.textAlign = "start"
        ctx.fillText((x_axis_distance_grid_lines-i)*y_axis_starting_point, // text
        x_axis_distance_px+8, // x-coordinate
        grid_size*i+3) // y-coordinate

      }

    }


    // ~~*~~ DRAW POINT ON GRID ~~*~~ //

    // Draw point on canvas (single point drawn where the user clicks)
    function drawPoint(
      event, // event (mousedown)
      ctx // 2d context of canvas
    ) {

      ctx.strokeStyle = "red"
      ctx.lineJoin = "round"
      ctx.lineCap = "round"
      ctx.lineWidth = 5

      ctx.beginPath()
      ctx.moveTo(event.offsetX, event.offsetY)
      ctx.lineTo(event.offsetX, event.offsetY)
      ctx.stroke()

    }


    // Use object of point coordinates to draw them on canvas
    function drawPointsfromObject(
      coordsObj, // canvas coordinates
      ctx // 2d context of the canvas
    ) {

      ctx.strokeStyle = "red"
      ctx.lineJoin = "round"
      ctx.lineCap = "round"
      ctx.lineWidth = 5

      coordsObj.forEach(point => {
        ctx.beginPath()
        ctx.moveTo(point.x, point.y)
        ctx.lineTo(point.x, point.y)
        ctx.stroke()

      })

    }


    // ~~*~~ DISPLAY POINT COORDINATES ON TABLE ~~*~~ //

    function displayData(
      table,
      newCoords
    ) {

      const row = document.createElement("tr")

      row.innerHTML = `
      <td>${newCoords[0]}<td>
      <td>${newCoords[1]}<td>
      `
      table.appendChild(row)

    }

    // ~~*~~ DRAW LINE OF BEST FIT ~~*~~ //

    // Draw fitted line
    function drawFittedLine(
      canvas, // canvas
      ctx, // 2d context of canvas
      intercept, // y-intercept as estimated by OLS
      slopeCoef, // slope coefficient as estimated by OLS
      num_lines_x, // number of horizontal lines on the grid
      num_lines_y, // number of vertical lines on the grid
      x_axis_distance_grid_lines, // distance of main x axis from the top in terms of grid blocks (e.g. if 10, then the main x axis will be 10 grid blocks from the top.)
      y_axis_distance_grid_lines, // distance of main y axis from the left in terms of grid blocks (e.g. if 10, then the main y axis will be 10 grid blocks from the left.)
      xmin, // minimum value on the horizontal axis
      xmax, // maximum value on the horizontal axis
      ymax // maximum value on the vertical axis
    ) {
      console.log("here")
      ctx.strokeStyle = "blue"
      ctx.lineJoin = "round"
      ctx.lineCap = "round"
      ctx.lineWidth = 0.5

      let yFrom = -intercept - slopeCoef*xmin
      let yTo = -intercept - slopeCoef*xmax

      let fittedOrigin = toCanvasCoordinates(
        xmin,
        yFrom,
        canvas,
        num_lines_x,
        num_lines_y,
        x_axis_distance_grid_lines,
        y_axis_distance_grid_lines,
        xmax,
        ymax
      )
      let fittedDestination = toCanvasCoordinates(
        xmax,
        yTo,
        canvas,
        num_lines_x,
        num_lines_y,
        x_axis_distance_grid_lines,
        y_axis_distance_grid_lines,
        xmax,
        ymax
      )

      ctx.beginPath()
      ctx.moveTo(fittedOrigin[0], fittedOrigin[1])
      ctx.lineTo(fittedDestination[0], fittedDestination[1])
      ctx.stroke()
    }
