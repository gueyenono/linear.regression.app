// ~~*~~ Get all stastical results

function getStatsResults() {

  var statsResults

  Shiny.addCustomMessageHandler("estimates", function(mod){


    statsResults.olsIntercept = Number(mod.intercept)
    statsResults.olsSlopeCoef = Number(mod.slopeCoef)

    statsResults.push(
      {
        olsIntercept: Number(mod.intercept),
        olsSlopeCoef: Number(mod.slopeCoef)
      }
    )

  })

  return statsResults

}
