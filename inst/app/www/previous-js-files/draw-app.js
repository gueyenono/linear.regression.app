$(document).on("shiny:connected", () => {


  console.log("draw-app.js is loaded")

  // let grid = require("./drawGridHelpers.js")

  // MASTER FUNCTION

  function drawGrid(
    canvas,
    num_lines_x,
    num_lines_y,
    x_axis_distance_grid_lines,
    y_axis_distance_grid_lines
  ) {

    // Variable declaration
    const sideLength = canvas.width // (value set in stone) Canvas height and length (in pixels)
    let grid_size = sideLength / num_lines_x
    let x_axis_starting_point = 10
    let y_axis_starting_point = 10


    // Create new canvas + 2D context
    const ctx = canvas.getContext("2d")
    canvas.style.height = `${sideLength}px`
    canvas.style.width = `${sideLength}px`


    // Draw horizontal lines with specifications
    drawXLines(
      canvas,
      ctx,
      num_lines_x,
      x_axis_distance_grid_lines,
      grid_size
    )

    // Draw vertical lines with specifications
    drawYLines(
      canvas,
      ctx,
      num_lines_y,
      y_axis_distance_grid_lines,
      grid_size
    )

    // Draw horizontal ticks
    drawXTicks(
      ctx,
      num_lines_y,
      x_axis_starting_point,
      y_axis_distance_grid_lines,
      grid_size
    )

    // Draw vertical ticks
    drawYTicks(
      ctx,
      num_lines_x,
      y_axis_starting_point,
      x_axis_distance_grid_lines,
      grid_size
    )

  }

  // ~~ SUB-FUNCTIONS ~~ //

  // Convert canvas coordinates to XY coordinates
  function toXYCoordinates(canvasXCoord, canvasYCoord) {
    let propX = x_axis_distance_grid_lines / num_lines_x
    let propY = y_axis_distance_grid_lines / num_lines_y
    let xCoord = ((canvasXCoord-canvas.width*propX) * xmax) / (canvas.width*propX)
    let yCoord = -((canvasYCoord-canvas.height*propY) * ymax) / (canvas.height*propY)
    return [xCoord, yCoord]
  }

  function toCanvasCoordinates(xCoord, yCoord) {
    let propX = x_axis_distance_grid_lines / num_lines_x
    let propY = y_axis_distance_grid_lines / num_lines_y
    let canvasXCoord = ((propX*xCoord*canvas.width)/xmax) + (propX*canvas.width)
    let canvasYCoord = ((propY*yCoord*canvas.height)/ymax) + (propY*canvas.height)
    return [canvasXCoord, canvasYCoord]
  }

  // Get OLS estimates from R
  function getOLS() {
    Shiny.addCustomMessageHandler("estimates", function(mod){
      console.log(mod)
      let slopeCoef

      if(mod.slopeCoef === null){
        slopeCoef = 0.0
      } else {
        slopeCoef = Number(mod.slopeCoef).toFixed(3)
      }

      spanIntercept.innerHTML = Number(mod.intercept).toFixed(3)
      spanSlopeCoef.innerHTML = slopeCoef

      drawFittedLine(
        ctx,
        Number(mod.intercept),
        Number(mod.slopeCoef),
        xmin,
        xmax
      )
    })
  }

  // Draw line of best fit (OLS)
  function drawFittedLine(
    ctx,
    intercept,
    slopeCoef,
    xmin,
    xmax
  ) {
    ctx.strokeStyle = "blue"
    ctx.lineJoin = "round"
    ctx.lineCap = "round"
    ctx.lineWidth = 0.5

    let yFrom = -intercept - slopeCoef*xmin
    let yTo = -intercept - slopeCoef*xmax

    let fittedOrigin = toCanvasCoordinates(xmin, yFrom)
    let fittedDestination = toCanvasCoordinates(xmax, yTo)

    console.log(fittedOrigin)

    ctx.beginPath()
    ctx.moveTo(fittedOrigin[0], fittedOrigin[1])
    ctx.lineTo(fittedDestination[0], fittedDestination[1])
    ctx.stroke()
  }

  // Display current mouse coordinates
  function displayMouseCoords(event) {
    let xyCoords = toXYCoordinates(event.offsetX, event.offsetY)
    mouseX.innerHTML =  xyCoords[0].toFixed(2)
    mouseY.innerHTML = xyCoords[1].toFixed(2)
  }

  // Draw points on canvas
  function drawPoint(event, ctx) {

    ctx.strokeStyle = "red"
    ctx.lineJoin = "round"
    ctx.lineCap = "round"
    ctx.lineWidth = 5

    ctx.beginPath()
    ctx.moveTo(event.offsetX, event.offsetY)
    ctx.lineTo(event.offsetX, event.offsetY)
    ctx.stroke()

  }

  // Append array with point coordinates and send them to R server
  function updateAndSendCoordinates(event) {

    // Note to future self: 0.5 here refers to the distance of main axis (currently 10) divided by number of grid lines (currently 20)
    // THe description above is currently true for both x and y axes
    let xCoord = ((event.offsetX-canvas.width*0.5) * xmax) / (canvas.width*0.5)
    let yCoord = -((event.offsetY-canvas.height*0.5) * ymax) / (canvas.height*0.5)

    coordinates.push({x: xCoord, y: yCoord})

    const table = document.querySelector("#table-coords")
    const row = document.createElement("tr")

    row.innerHTML = `
    <td>${coordinates[coordinates.length-1].x}<td>
    <td>${coordinates[coordinates.length-1].y}<td>
    `
    table.appendChild(row)
  }



  // Draw horizontal lines
  function drawXLines(
    canvas,
    ctx,
    num_lines_x,
    x_axis_distance_grid_lines,
    grid_size
  ) {

    for(let i = 0; i <= num_lines_x; i++){

      ctx.beginPath()
      ctx.lineWidth = 1

      if(i === x_axis_distance_grid_lines) ctx.strokeStyle = "#000000" // If line represents x-axis, draw in different color
      else ctx.strokeStyle = "#e9e9e9"

      if(i === num_lines_x) {
        ctx.moveTo(0, grid_size*i)
        ctx.lineTo(canvas.width, grid_size*i)
      } else {
        ctx.moveTo(0, grid_size*i + 0.5)
        ctx.lineTo(canvas.width, grid_size*i + 0.5)
      }

      ctx.stroke()
    }
  }

  // Draw vertical lines
  function drawYLines(
    canvas,
    ctx,
    num_lines_y,
    y_axis_distance_grid_lines,
    grid_size
  ) {
    for(let i = 0; i <= num_lines_y; i++){

      ctx.beginPath()
      ctx.lineWidth = 1

      // If line represents x-axis, draw in different color
      if(i === y_axis_distance_grid_lines) ctx.strokeStyle = "#000000"
      else ctx.strokeStyle = "#e9e9e9"

      if(i === num_lines_y) {
        ctx.moveTo(grid_size*i, 0)
        ctx.lineTo(grid_size*i, canvas.height)
      } else {
        ctx.moveTo(grid_size*i + 0.5, 0)
        ctx.lineTo(grid_size*i + 0.5, canvas.height)
      }

      ctx.stroke()
    }
  }

  // Draw horizontal tick marks
  function drawXTicks(
    ctx,
    num_lines_y,
    x_axis_starting_point,
    y_axis_distance_grid_lines,
    grid_size
  ) {

    let y_axis_distance_px = y_axis_distance_grid_lines*grid_size

    // Tick marks along the positive x-axis
    for(let i = 1; i < (num_lines_y - y_axis_distance_grid_lines); i++) {

      let tick_coord_x = y_axis_distance_px + grid_size*i

      ctx.beginPath()
      ctx.lineWidth = 1
      ctx.strokeStyle = "#000000"

      // Each tick mark is 6px long (-3 to 3)
      ctx.moveTo(tick_coord_x + 0.5, y_axis_distance_px-3)
      ctx.lineTo(tick_coord_x + 0.5, y_axis_distance_px+3)
      ctx.stroke()

      // Text value at that point
      ctx.font = "9px Arial"
      // ctx.textAlign = "start"
      ctx.fillText(x_axis_starting_point*i, // text
        tick_coord_x - 1, // x-coordinate
        y_axis_distance_px + 15) // y-coordinate

      }

      // Tick marks along the negative x-axis
      for(let i=1; i < y_axis_distance_grid_lines; i++) {

        ctx.beginPath()
        ctx.lineWidth = 1
        ctx.strokeStyle = "#000000"

        // Each tick mark is 6px long (-3 to 3)
        ctx.moveTo(grid_size*i + 0.5, y_axis_distance_px-3)
        ctx.lineTo(grid_size*i + 0.5, y_axis_distance_px+3)
        ctx.stroke()

        // Text value at that point
        ctx.font = "9px Arial"
        // ctx.textAlign = "end"
        ctx.fillText(-(y_axis_distance_grid_lines-i)*x_axis_starting_point, // text
        grid_size*i - 3, // x-coordinate
        y_axis_distance_px + 15) // y-coordinate
      }
    }

    // Draw vertical tick marks
    function drawYTicks(
      ctx,
      num_lines_x,
      y_axis_starting_point,
      x_axis_distance_grid_lines,
      grid_size
    ) {

      let x_axis_distance_px = x_axis_distance_grid_lines * grid_size

      // Tick marks along the negative y-axis
      for(let i = 1; i < (num_lines_x - x_axis_distance_grid_lines); i++) {

        let tick_coord_y = x_axis_distance_px + grid_size*i

        ctx.beginPath()
        ctx.lineWidth = 1
        ctx.strokeStyle = "#000000"

        // Each tick mark is 6px long (-3 to 3)
        ctx.moveTo(x_axis_distance_px-3, tick_coord_y+0.5)
        ctx.lineTo(x_axis_distance_px+3, tick_coord_y+0.5)
        ctx.stroke()

        // Text value at that point
        ctx.font = "9px Arial"
        // ctx.textAlign = "start"
        ctx.fillText(-i*y_axis_starting_point, // text
          x_axis_distance_px+8, // x-coordinate
          tick_coord_y+3) // y-coordinate

        }

        // Tick marks along the positive y-axis
        for(let i = 1; i < x_axis_distance_grid_lines; i++) {

          ctx.beginPath()
          ctx.lineWidth = 1
          ctx.strokeStyle = "#000000"

          // Each tick mark is 6px long (-3 to 3)
          ctx.moveTo(x_axis_distance_px-3, grid_size*i + 0.5)
          ctx.lineTo(x_axis_distance_px+3, grid_size*i + 0.5)
          ctx.stroke()

          // Text value at that point
          ctx.font = "9px Arial"
          // ctx.textAlign = "start"
          ctx.fillText((x_axis_distance_grid_lines-i)*y_axis_starting_point, // text
          x_axis_distance_px+8, // x-coordinate
          grid_size*i+3) // y-coordinate

        }

      }


      // Node elements
      const canvas = document.querySelector("#graph-draw-app")
      canvas.width = 800
      canvas.height = 800
      const ctx = canvas.getContext("2d")
      const mouseX = document.querySelector("#mouse-x-coord")
      const mouseY = document.querySelector("#mouse-y-coord")
      const spanIntercept = document.querySelector("#intercept")
      const spanSlopeCoef = document.querySelector("#slope-coef")

      // Empty coordinates object
      let coordinates = []
      let [xmin, xmax] = [-100, 100]
      let [ymin, ymax] = [-100, 100]

      // Draw grid
      let num_lines_x = 20 // Number of horizontal lines on the grid
      let num_lines_y = 20 // Number of vertical lines on the grid
      let x_axis_distance_grid_lines = num_lines_x / 2
      let y_axis_distance_grid_lines = num_lines_y / 2


      /// TEST

      // canvas.addEventListener("mousedown", (event) => {
      //   console.log(`
      //     Canvas X: ${event.offsetX}
      //     Canvas Y: ${event.offsetY}
      //   `)
      // })
      ////


      drawGrid(
        canvas,
        num_lines_x,
        num_lines_y,
        x_axis_distance_grid_lines,
        y_axis_distance_grid_lines
      )

      // Events

      // Display current mouse coordinates on the canvas
      canvas.addEventListener("mousemove", (event) => {
        displayMouseCoords(event)
      })

      // When user clicks on canvas
      canvas.addEventListener("mousedown", (event) => {

        drawPoint(event, ctx)  // Draw point where user clicks
        updateAndSendCoordinates(event)  // Update coordinates object
        Shiny.setInputValue("data_coords", JSON.stringify(coordinates))  // Send coordinates to R
        getOLS()

      })

    })
