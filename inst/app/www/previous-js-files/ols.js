// $(document).on("shiny:connected", () => {
//
//   // DOM elements
//
//   console.log("ols.js is loaded")
//
//   const canvas = document.querySelector("#graph-draw-app")
//   let spanIntercept = document.querySelector("#intercept")
//   let spanSlopeCoef = document.querySelector("#slope-coef")
//
//   console.log(canvas)
//
//   // Functions
//
//   // Draw line of best fit
//   function drawFittedLine(
//     ctx,
//     intercept,
//     slopeCoef,
//     xmin,
//     xmax
//   ) {
//     ctx.strokeStyle = "blue"
//     ctx.lineJoin = "round"
//     ctx.lineCap = "round"
//     ctx.lineWidth = 5
//
//     let yFrom = intercept + slopeCoef*xmin
//     let yTo = intercept + slopeCoef*xmax
//
//     ctx.beginPath()
//     ctx.moveTo(xmin, yFrom)
//     ctx.lineTo(xmax, yTo)
//     ctx.stroke()
//   }
//
//
//   canvas.addEventListener("mousedown", () => {
//     Shiny.addCustomMessageHandler("estimates", function(mod){
//
//       // Conditional statement for slope coefficient
//       let slopeCoef
//
//       if(mod.slopeCoef === null){
//         slopeCoef = 0.0
//       } else {
//         slopeCoef = Number(mod.slopeCoef).toFixed(3)
//       }
//
//       spanIntercept.innerHTML = Number(mod.intercept).toFixed(3)
//       spanSlopeCoef.innerHTML = slopeCoef
//     })
//   })
//
//
// })
