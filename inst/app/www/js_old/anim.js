// document.addEventListener("DOMContentLoaded", () => {

console.log("anim.js is loaded")

// Method for animating the pictures
toggleClass = function(appPicClass){

  let pElements = appPics.querySelectorAll(`${appPics} p`)

  pElements.forEach(p => {

    let classes = [...p.classList]
    let currentState = classes[1]

    let reg = new RegExp("in", "gi")
    let isInvisible = currentState.match(reg)

    let visibilityClass = classes.pop()

    if(isInvisible){
      p.classList.remove(visibilityClass)
      p.classList.add(`${classes}-visible`)
    } else {
      p.classList.remove(visibilityClass)
      p.classList.add(`${classes}-invisible`)
    }

  })

}
