let obj = [
  {x: 1, y: 3},
  {x: 2, y: 5},
  {x: 7, y: 12}
]

obj.forEach(element => {
  console.log(`x: ${element.x}\ny: ${element.y}\n\n`)
})

console.table(obj)
