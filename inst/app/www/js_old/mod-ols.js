// FUNCTIONS FOR ORDINARY LEAST SQUARES


Shiny.addCustomMessageHandler("estimates", function(mod){

  // Conditional statement for slope coefficient
  let slopeCoef

  if(mod.slopeCoef === null){
    slopeCoef = 0.0
  } else {
    slopeCoef = Number(mod.slopeCoef).toFixed(3)
  }

  spanIntercept.innerHTML = Number(mod.intercept).toFixed(3)
  spanSlopeCoef.innerHTML = slopeCoef
})
