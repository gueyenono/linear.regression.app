// UTILITY FUNCTIONS USED IN OTHER MODULES

// ~~*~~ Coordinates conversion ~~*~~ //

// Convert canvas coordinates to XY coordinates
function toXYCoordinates(canvasXCoord, canvasYCoord) {
  let propX = x_axis_distance_grid_lines / num_lines_x
  let propY = y_axis_distance_grid_lines / num_lines_y
  let xCoord = ((canvasXCoord-propX*canvas.width) * xmax) / (propX*canvas.width)
  let yCoord = -((canvasYCoord-propY*canvas.height) * ymax) / (propY*canvas.height)
  return [xCoord, yCoord]
}

// Convert XY coordinates to canvas coordinates
function toCanvasCoordinates(xCoord, yCoord) {
  let propX = x_axis_distance_grid_lines / num_lines_x
  let propY = y_axis_distance_grid_lines / num_lines_y
  let canvasXCoord = ((propX*xCoord*canvas.width)/xmax) + (propX*canvas.width)
  let canvasYCoord = ((propY*yCoord*canvas.height)/ymax) + (propY*canvas.height)
  return [canvasXCoord, canvasYCoord]
}
