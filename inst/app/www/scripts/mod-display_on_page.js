// FUNCTIONS FOR DISPLAYING ON THE WEBPAGE AND OTHER ANIMATIONS

// ~~*~~ DEPENDENCIES ~~*~~ //

// toXYCoordinates()

// Display current mouse coordinates
function displayMouseCoords(clickEvent, xDisplayElement, yDisplayElement) {
  let xyCoords = toXYCoordinates(clickEvent.offsetX, clickEvent.offsetY)
  xDisplayElement.innerHTML =  xyCoords[0].toFixed(2)
  yDisplayElement.innerHTML = xyCoords[1].toFixed(2)
}


// Quick animation
// const appPics = document.querySelectorAll(".app-pic")
// let pElements = this.querySelectorAll("p")
function toggleClass(pElements) {

  pElements.forEach(p => {

    let classes = [...p.classList]
    let currentState = classes[1]

    let reg = new RegExp("in", "gi")
    let isInvisible = currentState.match(reg)

    let visibilityClass = classes.pop()

    if(isInvisible){
      p.classList.remove(visibilityClass)
      p.classList.add(`${classes}-visible`)
    } else {
      p.classList.remove(visibilityClass)
      p.classList.add(`${classes}-invisible`)
    }

  })
}

// Test function
function showAlert(msg) {
  alert(msg)
}
