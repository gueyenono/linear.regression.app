// FUNCTIONS FOR DRAWING AN XY COORDINATE SYSTEM, POINTS AND LINES

// ~~*~~ DEPENDENCIES ~~*~~ //

// toCanvasCoordinates()

// ~~*~~ DRAW BASE GRID ~~*~~ //

// Draw grid
function drawGrid(
  canvas, // canvas element
  ctx, // 2D context of canvas
  num_lines_x, // Desired number of x lines in the grid
  num_lines_y, // Desired number of y lines in the grid
  x_axis_distance_grid_lines, // distance of main x axis from the top in terms of grid blocks (e.g. if 10, then the main x axis will be 10 grid blocks from the top.)
  y_axis_distance_grid_lines, // distance of main y axis from the left in terms of grid blocks (e.g. if 10, then the main y axis will be 10 grid blocks from the left.)
  x_axis_starting_point,
  y_axis_starting_point
) {

  // Variable declaration
  const sideLength = canvas.width // (value set in stone) Canvas height and length (in pixels)
  let grid_size = sideLength / num_lines_x
  // let x_axis_starting_point = 10
  // let y_axis_starting_point = 10


  // Store canvas dimensions
  canvas.style.height = `${sideLength}px`
  canvas.style.width = `${sideLength}px`


  // Draw horizontal lines with specifications
  drawXLines(
    canvas,
    ctx,
    num_lines_x,
    x_axis_distance_grid_lines,
    grid_size
  )

  // Draw vertical lines with specifications
  drawYLines(
    canvas,
    ctx,
    num_lines_y,
    y_axis_distance_grid_lines,
    grid_size
  )

  // Draw horizontal ticks
  drawXTicks(
    ctx,
    num_lines_y,
    x_axis_starting_point,
    y_axis_distance_grid_lines,
    grid_size
  )

  // Draw vertical ticks
  drawYTicks(
    ctx,
    num_lines_x,
    y_axis_starting_point,
    x_axis_distance_grid_lines,
    grid_size
  )

}

// Draw horizontal lines
function drawXLines(
  canvas,
  ctx,
  num_lines_x,
  x_axis_distance_grid_lines,
  grid_size
) {

  for(let i = 0; i <= num_lines_x; i++){

    ctx.beginPath()
    ctx.lineWidth = 1

    if(i === x_axis_distance_grid_lines) ctx.strokeStyle = "#000000" // If line represents x-axis, draw in different color
    else ctx.strokeStyle = "#e9e9e9"

    if(i === num_lines_x) {
      ctx.moveTo(0, grid_size*i)
      ctx.lineTo(canvas.width, grid_size*i)
    } else {
      ctx.moveTo(0, grid_size*i + 0.5)
      ctx.lineTo(canvas.width, grid_size*i + 0.5)
    }

    ctx.stroke()
  }
}

// Draw vertical lines
function drawYLines(
  canvas,
  ctx,
  num_lines_y,
  y_axis_distance_grid_lines,
  grid_size
) {
  for(let i = 0; i <= num_lines_y; i++){

    ctx.beginPath()
    ctx.lineWidth = 1

    // If line represents x-axis, draw in different color
    if(i === y_axis_distance_grid_lines) ctx.strokeStyle = "#000000"
    else ctx.strokeStyle = "#e9e9e9"

    if(i === num_lines_y) {
      ctx.moveTo(grid_size*i, 0)
      ctx.lineTo(grid_size*i, canvas.height)
    } else {
      ctx.moveTo(grid_size*i + 0.5, 0)
      ctx.lineTo(grid_size*i + 0.5, canvas.height)
    }

    ctx.stroke()
  }
}

// Draw horizontal tick marks
function drawXTicks(
  ctx,
  num_lines_y,
  x_axis_starting_point,
  y_axis_distance_grid_lines,
  grid_size
) {

  let y_axis_distance_px = y_axis_distance_grid_lines*grid_size

  // Tick marks along the positive x-axis
  for(let i = 1; i < (num_lines_y - y_axis_distance_grid_lines); i++) {

    let tick_coord_x = y_axis_distance_px + grid_size*i

    ctx.beginPath()
    ctx.lineWidth = 1
    ctx.strokeStyle = "#000000"

    // Each tick mark is 6px long (-3 to 3)
    ctx.moveTo(tick_coord_x + 0.5, y_axis_distance_px-3)
    ctx.lineTo(tick_coord_x + 0.5, y_axis_distance_px+3)
    ctx.stroke()

    // Text value at that point
    ctx.font = "9px Arial"
    // ctx.textAlign = "start"
    ctx.fillText(x_axis_starting_point*i, // text
      tick_coord_x - 1, // x-coordinate
      y_axis_distance_px + 15) // y-coordinate

    }

    // Tick marks along the negative x-axis
    for(let i=1; i < y_axis_distance_grid_lines; i++) {

      ctx.beginPath()
      ctx.lineWidth = 1
      ctx.strokeStyle = "#000000"

      // Each tick mark is 6px long (-3 to 3)
      ctx.moveTo(grid_size*i + 0.5, y_axis_distance_px-3)
      ctx.lineTo(grid_size*i + 0.5, y_axis_distance_px+3)
      ctx.stroke()

      // Text value at that point
      ctx.font = "9px Arial"
      // ctx.textAlign = "end"
      ctx.fillText(-(y_axis_distance_grid_lines-i)*x_axis_starting_point, // text
      grid_size*i - 3, // x-coordinate
      y_axis_distance_px + 15) // y-coordinate
    }
  }

  // Draw vertical tick marks
  function drawYTicks(
    ctx,
    num_lines_x,
    y_axis_starting_point,
    x_axis_distance_grid_lines,
    grid_size
  ) {

    let x_axis_distance_px = x_axis_distance_grid_lines * grid_size

    // Tick marks along the negative y-axis
    for(let i = 1; i < (num_lines_x - x_axis_distance_grid_lines); i++) {

      let tick_coord_y = x_axis_distance_px + grid_size*i

      ctx.beginPath()
      ctx.lineWidth = 1
      ctx.strokeStyle = "#000000"

      // Each tick mark is 6px long (-3 to 3)
      ctx.moveTo(x_axis_distance_px-3, tick_coord_y+0.5)
      ctx.lineTo(x_axis_distance_px+3, tick_coord_y+0.5)
      ctx.stroke()

      // Text value at that point
      ctx.font = "9px Arial"
      // ctx.textAlign = "start"
      ctx.fillText(-i*y_axis_starting_point, // text
        x_axis_distance_px+8, // x-coordinate
        tick_coord_y+3) // y-coordinate

      }

      // Tick marks along the positive y-axis
      for(let i = 1; i < x_axis_distance_grid_lines; i++) {

        ctx.beginPath()
        ctx.lineWidth = 1
        ctx.strokeStyle = "#000000"

        // Each tick mark is 6px long (-3 to 3)
        ctx.moveTo(x_axis_distance_px-3, grid_size*i + 0.5)
        ctx.lineTo(x_axis_distance_px+3, grid_size*i + 0.5)
        ctx.stroke()

        // Text value at that point
        ctx.font = "9px Arial"
        // ctx.textAlign = "start"
        ctx.fillText((x_axis_distance_grid_lines-i)*y_axis_starting_point, // text
        x_axis_distance_px+8, // x-coordinate
        grid_size*i+3) // y-coordinate

      }

    }



    // ~~*~~ DRAW LINE OF BEST FIT ~~*~~ //

    // Draw fitted line
    function drawFittedLine(
      ctx,
      intercept,
      slopeCoef,
      xmin,
      xmax
    ) {
      ctx.strokeStyle = "blue"
      ctx.lineJoin = "round"
      ctx.lineCap = "round"
      ctx.lineWidth = 0.5

      let yFrom = -intercept - slopeCoef*xmin
      let yTo = -intercept - slopeCoef*xmax

      let fittedOrigin = toCanvasCoordinates(xmin, yFrom)
      let fittedDestination = toCanvasCoordinates(xmax, yTo)

      ctx.beginPath()
      ctx.moveTo(fittedOrigin[0], fittedOrigin[1])
      ctx.lineTo(fittedDestination[0], fittedDestination[1])
      ctx.stroke()
    }


    // ~~*~~ DRAW POINT ON GRID ~~*~~ //

    // Draw points on canvas
    function drawPoint(event, ctx) {

      ctx.strokeStyle = "red"
      ctx.lineJoin = "round"
      ctx.lineCap = "round"
      ctx.lineWidth = 5

      ctx.beginPath()
      ctx.moveTo(event.offsetX, event.offsetY)
      ctx.lineTo(event.offsetX, event.offsetY)
      ctx.stroke()

    }
